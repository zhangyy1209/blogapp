class Message < ActiveRecord::Base
  self.table_name = 'messages'

  validates_presence_of :name, :content
end
